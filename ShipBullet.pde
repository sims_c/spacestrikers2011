// Courtney Sims & Dong yoo Kang
// CS 4300
// Homework 3 - 2D Project
// 10/17/2011


// Represents a bullet shot from the player ship
class ShipBullet {
  float xpos;
  float ypos;
  int yspeed;
  PImage bulletPic;
  PImage explosionPic;
  boolean isFinished;
  static final int BULLET_WIDTH = 20;
  static final int BULLET_HEIGHT = 40;
  static final int HIT_BULLET_DISTANCE = 30;
  static final int BULLET_SPEED = 20;

  ShipBullet (float xpos, float ypos, String imageName, String explosionImageName) {
    this.xpos = xpos;
    this.ypos = ypos;
    this.yspeed = ShipBullet.BULLET_SPEED;
    this.bulletPic = loadImage(imageName);
    this.explosionPic = loadImage(explosionImageName);
    this.isFinished = false;
  }

  // returns whether this bullet is finished, and makes
  // the bullet finished if it is off the screen
  boolean bulletFinished() {
    if ((this.ypos <= 0) || (this.ypos >= height)) {
      this.finished();
    }
    return this.isFinished;
  }

  // makes this bullet finished
  void finished() {
    this.isFinished = true;
  }

  // shows an explosion effect on the screen
  void bombEffect() {
    imageMode(CENTER);
    image(this.explosionPic, this.xpos, this.ypos);
  }

  // displays this bullet if it is not finished
  void display() {
    if (!this.bulletFinished()) {
      imageMode(CENTER);
      image(this.bulletPic, this.xpos, this.ypos, ShipBullet.BULLET_WIDTH, ShipBullet.BULLET_HEIGHT);
    }
  }

  // moves this bullet on the screen
  void bulletMove() {
    if (!this.bulletFinished()) {
      this.ypos = this.ypos - this.yspeed;
    }
  }

  // checks whether this bullet has hit the given Alien
  boolean madeHit(Alien a) {
    float distance = dist(this.xpos, this.ypos, a.xPosition(), a.yPosition());
    return (distance < ShipBullet.HIT_BULLET_DISTANCE);
  }
}

