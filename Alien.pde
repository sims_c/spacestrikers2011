// Courtney Sims & Dong yoo Kang
// CS 4300
// Homework 3 - 2D Project
// 10/17/2011


// Represents an enemy alien
class Alien {
  float xpos;
  float ypos;
  float startX;
  float startY;
  float desx;
  float desy;
  float speed;
  int hp;
  int maxHP;
  boolean isDead;
  PImage alienPicture;
  ArrayList bullets;
  int fireInterval;
  float imageWidth;
  float imageHeight;

  Alien (float x, float y, float speed, String imageName, float imageWidth, float imageHeight) {
    this.xpos = x;
    this.ypos = y;
    this.startX = x;
    this.startY = y;
    this.desx = 10;
    this.desy = 10;
    this.speed = speed;
    this.hp = 5;
    this.maxHP = hp;
    this.isDead = false;
    this.alienPicture = loadImage(imageName);
    this.bullets = new ArrayList();
    this.fireInterval = 5;
    this.imageWidth = imageWidth;
    this.imageHeight = imageHeight;
  }

  // resets the health, position, and bullets of this Alien
  void reset() {
    this.hp = this.maxHP;
    this.isDead = false;
    this.xpos = this.startX;
    this.ypos = this.startY;
    this.bullets = new ArrayList();
  }

  // returns the max health of this alien
  int maxAlienHealth() {
    return this.maxHP;
  }

  // is this Alien destroyed?
  boolean isDestroyed() {
    return this.isDead;
  }

  // returns the current health of this Alien
  int health() {
    return this.hp;
  }

  // returns the current x coordinate of this Alien
  float xPosition() {
    return this.xpos;
  }

  // returns the current y coordinate of this Alien
  float yPosition() {
    return this.ypos;
  }

  // reduces this Alien's health by 1. If this Alien's health
  // is 0, this Alien is dead.
  void isHit() {
    if (this.hp <= 1) {
      this.hp = 0;
      this.isDead = true;
    } 
    else {
      this.hp--;
    }
  }

  // displays this Alien on the screen
  // changes the size of this Alien depending on its current remaining health
  void display() {
    if (!this.isDestroyed()) {
      imageMode(CENTER);
      float tempImageWidth = this.imageWidth;
      float tempImageHeight = this.imageHeight;

      if ((this.hp < this.maxHP) && (this.hp >= 3)) {
        tempImageWidth = (this.imageWidth / 5) * this.hp;
        tempImageHeight = (this.imageHeight / 5) * this.hp;
      }
      else if (this.hp < 3) {
        tempImageWidth = (this.imageWidth / 5) * 3;
        tempImageHeight = (this.imageHeight / 5) * 3;
      }
      image(this.alienPicture, this.xpos, this.ypos, tempImageWidth, tempImageHeight);
    }
  }

  // moves this Alien in a random path on the screen within its bounds
  void moveAlien() {
    if (Math.abs(this.ypos - this.desy) < this.speed) {
      this.desy = random(height/10, (height/3)*2);
    }
    if (Math.abs(this.xpos - this.desx) < this.speed) {
      this.desx = random((width/6), width-(width/6));
    }
    if (this.ypos <= this.desy) {
      this.ypos = this.ypos + this.speed;
    }
    if (this.ypos > this.desy) {
      this.ypos = this.ypos - this.speed;
    }
    if (this.xpos <= this.desx) {
      this.xpos = this.xpos + this.speed;
    }
    if (this.xpos > this.desx) {
      this.xpos = this.xpos - this.speed;
    }
  }

  // changes the rate at which this Alien fires. A smaller
  // number makes the Alien shoot slower; a higher number
  // makes the Alien shoot faster.
  void changeFireInterval(int n) {
    this.fireInterval = 5 - n;
  }

  // fires bullets from this Alien
  void fire() {
    int fireTime = millis() % this.fireInterval;
    if (!this.isDestroyed() && (fireTime == 0)) {
      this.bullets.add(new AlienBullet(this.xpos, this.ypos, "explosionmushroomlarge.gif"));
    }
  }

  // moves the bullets of this Alien. If the bullets hit the player
  // ship, the player ship loses health. If the player ship's health
  // is 0, the player ship is destroyed
  void hitShip(Spaceship ship) {
    int numBullets = this.bullets.size();
    for (int i = 0; i < numBullets; i++) {
      AlienBullet b = (AlienBullet) bullets.get(i);
      b.bulletMove();
      b.display();
      if (b.madeHit(ship) && !b.bulletFinished()) {
        ship.isHit();
        if (!ship.isDestroyed()) {
          b.bombEffect();
        }
        b.finished();
      }
    }
  }
}

