// Courtney Sims & Dong yoo Kang
// CS 4300
// Homework 3 - 2D Project
// 10/17/2011


// Contains preset fonts

void screenText1() {
  PFont font = loadFont("SegoeScript-48.vlw");
  textFont(font);
  fill(random(0, 255), random(0, 200), random(0, 200));
}

void screenText2() {
  PFont font = loadFont("SegoeScript-48.vlw");
  textFont(font);
  fill(255, random(0, 200), random(0, 200));
}

void screenText3() {
  PFont font = loadFont("SegoeScript-20.vlw");
  textFont(font);
  fill(255, 0, 0, 190);
}

void screenText4() {
  PFont font = loadFont("SansSerif.bolditalic-24.vlw"); 
  textFont(font); 
  fill(255, 255, 255);
}

