// Courtney Sims & Dong yoo Kang
// CS 4300
// Homework 3 - 2D Project
// 10/17/2011


// Represents a game world
class GameWorld {
  Spaceship ship;
  Alien alien1;
  Alien alien2;
  Alien alien3;
  boolean gameStarted;
  int triesLeft;
  boolean retry;
  int level;

  GameWorld (Spaceship ship, Alien alien1, Alien alien2, Alien alien3) {
    this.ship = ship;
    this.alien1 = alien1;
    this.alien2 = alien2;
    this.alien3 = alien3;
    this.gameStarted = false;
    this.triesLeft = 3;
    this.retry = false;
    this.level = 0;
  }

  // displays a background on the screen
  void showBackground(String dataName, float posx, float posy, float sizeX, float sizeY) {
    PImage background1 = loadImage(dataName);
    imageMode(CORNER);
    image(background1, posx, posy, sizeX, sizeY);
  }

  // Displays the start screen
  void showStartScreen() {
    screenText1();
    this.showBackground("startshot.png", 0, 0, width, height);
    textAlign(CENTER);
    text("Space Strikers 2011", width/6, height/6, (width/6) * 5, height/2);
    screenText4();
    fill(random(0, 255), 0, 0);
    text("Press & Hold SPACE to Start", width/2, (height/3) * 2, width/2, height/4);
  }

  /* Displays the level select screen.
   Sets the game level/difficulty according to the option
   that the user clicks on */
  void LevelSelectScreen() {

    rectMode(CORNERS);

    // the coordinates of the level select rectangles
    float rectY1 = (width/6);
    float rectY2 = (width/3);
    float rect1X1 = (width/16);
    float rect1X2 = 5 * rect1X1;
    float rect2X1 = 6 * rect1X1;
    float rect2X2 = 10 * rect1X1;
    float rect3X1 = 11 * rect1X1;
    float rect3X2 = 15 * rect1X1;

    this.showBackground("startshot.png", 0, 0, width, height);

    fill(0, random(200, 255), 0);
    rect(rect1X1, rectY1, rect1X2, rectY2);

    fill(random(200, 255), random(200, 255), 0);
    rect(rect2X1, rectY1, rect2X2, rectY2);

    fill(random(200, 255), 0, 0);
    rect(rect3X1, rectY1, rect3X2, rectY2);

    // sets rectMode back to the default
    rectMode(CORNER);

    screenText4();
    textAlign(CENTER);

    text("Rare", (rect1X1 + rect1X2)/2, (rectY1 + rectY2)/2);
    text("Medium", (rect2X1 + rect2X2)/2, (rectY1 + rectY2)/2);
    text("Well-Done", (rect3X1 + rect3X2)/2, (rectY1 + rectY2)/2);

    fill(0, 0, 0);
    text("CLICK & Hold The Level With The Mouse", width/6, height/2, (width/3)*2, height/2);

    if (mousePressed &&
      (mouseX >= rect1X1) && (mouseX <= rect1X2) &&
      (mouseY >= rectY1) && (mouseY <= rectY2)) {
      this.level = 1;
    }
    if (mousePressed &&
      (mouseX >= rect2X1) && (mouseX <= rect2X2) &&
      (mouseY >= rectY1) && (mouseY <= rectY2)) {
      this.level = 2;
    }
    if (mousePressed &&
      (mouseX >= rect3X1) && (mouseX <= rect3X2) &&
      (mouseY >= rectY1) && (mouseY <= rectY2)) {
      this.level = 3;
    }
  }

  // Displays the winning screen
  void showWinScreen() {    
    this.showBackground("startshot.png", 0, 0, width, height);
    screenText2();
    textAlign(CENTER);
    text("Thank you for saving the universe!", width/5, height/5, (width/5) * 4, (height/5) * 4);
    screenText4();
    fill(0, 0, 0);
    text("Made by Courtney Sims & DK", width/8, height/6, (width/8)*7, height/2);
  }

  // Displays the losing screen according to whether the player still has
  // tries remaining.
  void showLoseScreen() {
    this.showBackground("startshot.png", 0, 0, width, height);
    screenText2();
    textAlign(CENTER);
    text("Game Over.", width/4, height/5, (width/4)*3, height/2);
    if (this.triesLeft > 0) {
      text ("Continue? Press and hold 'y'.", width/4, height/2, (width/4)*3, height/2);
    }
    if (this.triesLeft == 0) {
      screenText4();
      fill(0, 0, 0);
      text("Made by Courtney Sims & DK", width/8, height/6, (width/8)*7, height/2);
    }
  }

  // checks whether the player has won the game
  // (if the player is still alive, and all the aliens are destroyed).
  boolean playerWon() {
    if (this.level == 1) {
      return (!this.ship.isDestroyed()) && this.alien1.isDestroyed();
    }
    else if (this.level == 2) {
      return (!this.ship.isDestroyed()) && this.alien1.isDestroyed()
        && this.alien2.isDestroyed();
    }
    else if (this.level == 3) {
      return (!this.ship.isDestroyed()) && this.alien1.isDestroyed()
        && this.alien2.isDestroyed() && this.alien3.isDestroyed();
    }
    else {
      return false;
    }
  }

  // checks whether the player has lost the game
  boolean playerLost() {
    return this.ship.isDestroyed();
  }

  // resets this game world so the player can restart the game
  // press 'y' to restart
  void retryGame() {
    if (this.playerLost() && (this.triesLeft != 0)) {
      if (keyPressed && (key == 'y' || key == 'Y')) {
        this.ship.reset();
        this.alien1.reset();
        this.alien2.reset();
        this.alien3.reset();
        this.gameStarted = false;
        this.retry = true;
      }
    }
  }

  // displays the player and enemy health on the screen
  void displayHP() {

    // total Alien health depending on level of difficulty
    // (there will always be at least 1 Alien)
    int totalAlienHealth = this.alien1.maxAlienHealth();
    if (this.level == 2) {
      totalAlienHealth = this.alien1.maxAlienHealth() + this.alien2.maxAlienHealth();
    }
    if (this.level == 3) {
      totalAlienHealth = this.alien1.maxAlienHealth() + this.alien2.maxAlienHealth() +
        this.alien3.maxAlienHealth();
    }

    float base = (width/10) * 9;
    float hpbar = base/totalAlienHealth;
    float wh = width/30;
    float he = height/90;
    float he2 = height/100;

    // shows the background for the Alien HP bar
    strokeWeight(20);
    strokeCap(ROUND);
    stroke(120, 120, 120);
    line(wh, he, width-wh, he);

    // shows damage taken on the Alien HP bar
    noStroke();
    fill(random(0, 255), 0, 0);
    rect(width/25, height/200, base, he2);

    // the remaining amount of total Alien health
    // depending on level of difficulty
    float alienHealthRemaining = 0;
    if (this.level == 1) {
      alienHealthRemaining = hpbar * this.alien1.health();
    }
    if (this.level == 2) {
      alienHealthRemaining = hpbar *(this.alien1.health() +
        this.alien2.health());
    }
    if (this.level == 3) {
      alienHealthRemaining = hpbar * (this.alien1.health()+
        this.alien2.health() + this.alien3.health());
    }

    // shows the remaining amount of total Alien health
    // in the Alien HP bar
    fill(255, 255, 0);  
    rect(width/25, height/200, alienHealthRemaining, he2);

    // shows the background for the Spaceship HP bar
    strokeWeight(10);
    stroke(120, 120, 120);
    line(wh * 25, height - (2 * he2), width - (2 * wh), height - (2 * he2));

    // shows damage taken on the Spaceship HP bar
    float pwh = width / 9;   //8.57;
    noStroke();
    fill(random(0, 255), 0, 0);
    rect((width/6) * 5, height - (2 * he2), pwh, (he2 / 2)); //8);   // width/1.19  // height-13

    // shows the remaining amount of Spaceship health
    // in the Spaceship HP bar
    fill(255, 255, 0);  
    float phpbar = pwh / this.ship.maxShipHealth();
    float pdamage = phpbar * this.ship.health();
    rect((width/6) * 5, height - (2 * he2), pdamage, (he2 / 2)); //8);

    // Labels for the Alien and Spaceship HP bars
    fill(255, 255, 255);
    text("Enemy", 50, 35);
    text("Player", (width/10) * 9, height - (4 * he2));  //width/1.11, height/1.02);
  }

  // displays the start screen, level select screen, or game depending
  // on whether the game has started
  void playGame() {

    // show the start screen if the game hasn't started
    if (!this.gameStarted) {
      if (keyPressed && (key == ' ')) {
        this.gameStarted = true;
      }
      else {
        this.showStartScreen();
      }
    }

    // if the game has started but a level has not been selected,
    // shows the level select screen
    else if (this.gameStarted) {
      if (this.level == 0) {
        LevelSelectScreen();
      }

      // if a level has been selected, show the game world
      else if (this.level != 0) {
        this.showBackground("bg-blinking-star.gif", 0, 0, width, height);
        this.ship.moveShip();
        this.ship.display();
        this.ship.fire();
        this.ship.showBullets();
        this.ship.decrementShootTime();

        // makes Alien 1 active
        this.alien1.moveAlien();
        this.alien1.display();
        this.alien1.changeFireInterval(this.level);
        this.alien1.fire();
        this.alien1.hitShip(this.ship);
        this.ship.hitAlien(this.alien1);

        // makes Alien 2 active if the level is 2 or greater
        if (this.level > 1) {
          this.alien2.moveAlien();
          this.alien2.display();
          this.alien2.changeFireInterval(this.level);
          this.alien2.fire();
          this.alien2.hitShip(this.ship);
          this.ship.hitAlien(this.alien2);
        }

        // makes Alien 3 active if the level is 3
        if (this.level > 2) {
          this.alien3.moveAlien();
          this.alien3.display();
          this.alien3.changeFireInterval(this.level);
          this.alien3.fire();
          this.alien3.hitShip(this.ship);
          this.ship.hitAlien(this.alien3);
        }

        this.displayHP();
      }
    }
  }

  // displays the game world each tick
  void onTick() {

    // if the player has won, show the win screen
    if (this.playerWon()) {
      this.showWinScreen();
    }

    // if the player has lost, show the lose screen
    else if (this.playerLost()) {
      this.showLoseScreen();
      if (this.triesLeft > 0) {
        this.retryGame();
      }
      // if the player retries, restart the game
      if (this.retry) {
        this.playGame();
        this.retry = false;
        this.triesLeft--;
      }
    }

    // play the game
    else {
      this.playGame();
    }
  }
}

