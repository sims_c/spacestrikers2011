// Courtney Sims & Dong yoo Kang
// CS 4300
// Homework 3 - 2D Project
// 10/17/2011


// Represents a player-controlled spaceship
class Spaceship {
  float xpos;
  float ypos;
  float startX;
  float startY;
  boolean isDead;
  int hp;
  int maxHP;
  PImage shipPicture;
  ArrayList bullets;
  float shootTime;
  AudioPlayer shootSound;

  Spaceship (float x, float y, String imageName) {
    this.xpos = x;
    this.ypos = y;
    this.startX = x;
    this.startY = y;
    this.isDead = false;
    this.hp = 3;
    this.maxHP = hp;
    this.shipPicture = loadImage(imageName);
    this.bullets = new ArrayList();
    this.shootTime = 5;
    this.shootSound = minim.loadFile("shoot.wav");
  }

  // resets the health, position, and bullets of this ship
  void reset() {
    this.hp = this.maxHP;
    this.isDead = false;
    this.xpos = this.startX;
    this.ypos = this.startY;
    this.bullets = new ArrayList();
  }

  // returns the max health of this ship
  int maxShipHealth() {
    return this.maxHP;
  }

  // is this spaceship destroyed?
  boolean isDestroyed() {
    return this.isDead;
  }

  // returns the health of this spaceship
  int health() {
    return this.hp;
  }

  // decreases this ship's shoot time
  void decrementShootTime() {
    this.shootTime--;
  }

  // reduces this ship's health by 1, and makes the ship
  // dead once health reaches 0
  void isHit() {
    if (this.hp <= 1) {
      this.hp = 0;
      this.isDead = true;
    } 
    else {
      this.hp--;
    }
  }

  // returns the current x coordinates of this spaceship
  float xPosition() {
    return this.xpos;
  }

  // returns the current y coordinates of this spaceship
  float yPosition() {
    return this.ypos;
  }

  // displays the current spaceship on the screen
  void display() {
    imageMode(CENTER);
    image(this.shipPicture, this.xpos, this.ypos);
  }

  // moves this ship to the current mouse coordinates within the screen
  // keeps the ship from moving into the top 1/4 of the screen
  void moveShip() {
    if ((mouseX > 0) && (mouseX < width)) {
      this.xpos = mouseX;
    }
    if ((mouseY > (height/4)) && (mouseY < height)) {
      this.ypos = mouseY;
    }
  }    

  // fires a bullet from this ship
  // shootTime is used to keep bullets from firing in a continuous
  // stream/line in case the user keeps the mouse button pressed down
  void fire() {
    if (mousePressed && (this.shootTime < 0.5)) {
      this.bullets.add(new ShipBullet(this.xpos, this.ypos, "M1.gif", "explosionmushroomlarge.gif"));
      this.shootSound.loop();
      this.shootSound.play();
      this.shootTime = 5;
    }
  }

  // moves and displays this ship's bullets
  void showBullets() {
    int numBullets = this.bullets.size();
    for (int i = 0; i < numBullets; i++) {
      ShipBullet b = (ShipBullet) bullets.get(i);
      b.bulletMove();
      b.display();
    }
  }

  // If the bullets hit the given Alien, the Alien loses
  // health. If the Alien's health is 0, the Alien is
  // destroyed
  void hitAlien(Alien alien) {
    int numBullets = this.bullets.size();
    for (int i = 0; i < numBullets; i++) {
      ShipBullet b = (ShipBullet) bullets.get(i);

      if (b.madeHit(alien) && !b.bulletFinished()) {
        alien.isHit();
        if (!alien.isDestroyed()) {
          b.bombEffect();
        }
        b.finished();
      }
    }
  }
}

