// Courtney Sims & Dong yoo Kang
// CS 4300
// Homework 3 - 2D Project
// 10/17/2011


// Represents a bullet shot from an Alien
class AlienBullet {
  float xpos;
  float ypos;
  int yspeed;
  PImage explosionPic;
  boolean isFinished;
  static final int BULLET_WIDTH = 8;
  static final int BULLET_HEIGHT = 40;
  static final int HIT_BULLET_DISTANCE = 20;
  static final int BULLET_SPEED = 10;

  AlienBullet (float xpos, float ypos, String explosionImageName) {
    this.xpos = xpos;
    this.ypos = ypos;
    this.yspeed = AlienBullet.BULLET_SPEED;
    this.explosionPic = loadImage(explosionImageName);
    this.isFinished = false;
  }

  // returns whether this bullet is finished, and makes
  // the bullet finished if it is off the screen
  boolean bulletFinished() {
    if ((this.ypos <= 0) || (this.ypos >= height)) {
      this.finished();
    }
    return this.isFinished;
  }

  // makes this bullet finished
  void finished() {
    this.isFinished = true;
  }

  // shows an explosion effect on the screen
  void bombEffect() {
    imageMode(CENTER);
    image(this.explosionPic, this.xpos, this.ypos);
  }

  // displays this bullet if it is not finished
  void display() {
    if (!this.bulletFinished()) {
      noStroke();
      fill(0, 0, random(200, 255), 120);
      ellipse(this.xpos, this.ypos, AlienBullet.BULLET_WIDTH, AlienBullet.BULLET_HEIGHT);
    }
  }

  // moves this bullet on the screen
  void bulletMove() {
    if (!this.bulletFinished()) {
      this.ypos = this.ypos + this.yspeed;
    }
  }

  // checks whether this bullet has hit the given player ship
  boolean madeHit(Spaceship s) {
    float distance = dist(this.xpos, this.ypos, s.xPosition(), s.yPosition());
    return (distance < AlienBullet.HIT_BULLET_DISTANCE);
  }
}

