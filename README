README.txt

Courtney Sims & Dong yoo Kang
CS 4300
Homework 3 - 2D Project
10/17/2011

=========

Downloading the Processing program & running for the first time:

First, download Processing from http://processing.org/download/ .
Processing is available for Windows, OS X, and Linux. It downloads for
Linux/Windows as a .tgz/.zip file (respectively).  You must extract the
compressed folder, but besides that there is no installation and no admin
priveleges are required.

To open Processing, navigate to the folder you just extracted and double-click
on the Processing executable (it is a shellscript in Linux, and you must
select "Run" if it brings up a prompt).  If it is your first time running it,
you may be asked to pick a location for your "Sketchbook".  Any location is
okay, as running the project code does not depend on this at all.

=========

Executing the 2D Project game code:
---------
1. Open Processing, and click File > Open (or click the icon with the up arrow
     and select Open) to open the file browser
2. Browse to the "spacestrikers2011/" folder, and open the
     spacestrikers2011.pde file (this may open all the files at once)
3. Click the Play icon, or click Sketch > Run, to run the code.

Resolution and framerate options:
----------
Within the setup() function in the spacestrikers2011.pde file, there are
options to change the window size and framerate by commenting/uncommenting
according to the file.  We recommend size(600, 900) and frameRate(100)
(or greater) for higher resolution screens and/or faster computers.  We
recommend size(400, 60) and frameRate(30) (or lower) for lower resolution
screens and/or slower computers.


User actions at a glance:
----------
- On the start screen:
        - Press and hold the space bar to start
- On the level select screen:
        - Press and hold the left mouse button on a level to select it and start
          the game
- On the gameplay screen:
        - Click (or press and hold) the left mouse button to shoot
        - Move the mouse around the screen to move the spaceship
        - Press "p" to pause and unpause the game
- On the lose screen:
        - If you have tries remaining, you will be instructed to press "y" to
          start over

=========

Additional notes:
---------
Due to time constraints, there were functions that were originally in the
preliminary functional specification which are not present or were changed
in the final game.  The levels of difficulty vary with the number of aliens
and the speed at which the aliens shoot, instead of the speed at which they
move or the size of their hittable area.  The player name input screen and the
2nd type of missile were removed, and the player does not have a time limit
to choose to restart the game while on the losing screen.

Functionality that was added in the final game includes scaling the size of
the aliens based on their amount of remaining health.  Additionally,
spaceship movement and firing is now based on mouse input rather than keyboard
input.

=========

CREDITS FOR IMAGES & SOUNDS USED:
---------

bgSounds.mp3 :
http://www.4shared.com/get/PPKpopkT/Striker_1945__-_Overcoat_techn.html

shoot.wav :
Credit to one of DK's friends

bg-blinking-star.gif :
http://i619.photobucket.com/albums/tt272/ronqing/bg-blinking-star.gif

startshot.png :
http://www.wallpaper4me.com/wallpaper/Endless-Deep-Space/

M1.gif :
http://www.1001-votes.com/vote/4253sor/armes/missile-8.gif

explosionmushroomlarge.gif :
http://www.majhost.com/gallery/esreveR/SpriteAnimations/MetalSlug/

The spaceship and enemy gifs were found on the following page:
http://www.spriters-resource.com/community/showthread.php?tid=7065&pid=327872

Direct links are as follows:

S1.gif :
http://www.majhost.com/gallery/esreveR/Random/ketsuiwyvernii.gif

A1.gif :
http://www.majhost.com/gallery/esreveR/Random/envystand.gif

A2.gif :
http://www.majhost.com/gallery/esreveR/Random/bluntturret.gif

A3.gif :
http://www.majhost.com/gallery/esreveR/Random/sobutvulcans.gif


