// Courtney Sims & Dong yoo Kang
// CS 4300
// Homework 3 - 2D Project
// 10/17/2011

// used to play audio files
import ddf.minim.*;

//Declare audio play variables
Minim minim;
AudioPlayer player;

// Declare variables
boolean paused;
Alien alien1, alien2, alien3;
Spaceship ship;
GameWorld world;
float alien1ImageWidth;
float alien1ImageHeight;
float alien2ImageWidth;
float alien2ImageHeight;
float alien3ImageWidth;
float alien3ImageHeight;

// Initializes the game world
void setup() {

  /////////////////////////////////////////////////////////////////////////
  //  Recommended for higher resolution screens and/or faster computers  //
  //  Uncomment/comment to enable/disable                                //
  //                                                                     //
  size(600, 900);
  frameRate(190);
  /////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  //  Recommended for smaller resolution screens and/or slower computers  //
  //  Uncomment/comment to enable/disable                                 //
  //                                                                      //
  //size(400, 600);
  //frameRate(30);
  //////////////////////////////////////////////////////////////////////////

  minim = new Minim(this);
  player = minim.loadFile("bgSounds.mp3");
  player.play();

  colorMode(RGB);
  smooth();
  paused = false;

  // these image widths/heights are based off the image sizes
  alien1ImageWidth = 174;
  alien1ImageHeight = 180;
  alien2ImageWidth = 216;
  alien2ImageHeight = 197;
  alien3ImageWidth = 148;
  alien3ImageHeight = 167;

  alien1 = new Alien(width/4, height/4, 10, "A1.gif", alien1ImageWidth, alien1ImageHeight);
  alien2 = new Alien(width/2, height/2, 10, "A2.gif", alien2ImageWidth, alien2ImageHeight);
  alien3 = new Alien((width/4)*3, height/4, 10, "A3.gif", alien3ImageWidth, alien3ImageHeight);
  ship = new Spaceship(width/2, (height/4)*3, "S1.gif");
  world = new GameWorld(ship, alien1, alien2, alien3);
}

// Draws the game world
void draw() {
  world.onTick();
}


// Pauses and unpauses the game by pressing 'p'
void keyPressed() {
  if (keyPressed && (key == 'p' || key == 'P')) {
    paused = !paused;
    if (paused) {
      noLoop();
    } 
    else {
      loop();
    }
  }
}

